'use strict';
var _code = {
    /***********************前端自定义提示码***********************/
    "againLogin": "登录信息错误",
    "addSuccess": "新增成功",
    "saveSuccess": "保存成功",
    "deleteSuccess": "删除成功",
    "updateSuccess": "修改成功",
    "submitSuccess": "提交成功",
    "logoutSuccess": "退出成功",
    "noData": "数据库无对应数据",
    "dataError": "数据异常，请刷新页面，并联系客服，",
    "reqUrlError": "请求路径错误，可能没有请求权限",
    "reqSystemError": "服务器异常或连接不到",
    "reqHttpError": "请求失败，HTTP错误码：",
    "systemError": "系统异常，请联系管理员",
    "priceError": "价格未输入或格式错误，请检查",
    "intError": "价格格式错误，请检查",
    "passwordIsNull": "请输入密码",
    "startTime" : "请输入开始时间",
    "endTime" : "请输入结束时间",
    /***无需命名或难以命名的提示语句***/
    "451": "车辆已发车",
    "452": "请先配置司机",
    "453": "请输入线路名称",
    "454": "请选择城市",
    "455": "请选择订单类型",
    "501": "请求或返回错误，请联系管理员",
    //服务城市
    "550" : "请至少选择一项服务",
    "551" : "请先提交当天正在编辑或新增的时段",
    "552" : "結束时间必须大于开始时间",
    "553" : "请输入结束时间",
    "554" : "请输入计价规则",
    "555" : "请选择区县",
    /***************************后台提示码*************************/
    /*响应成功 统一返回 200*/
    "200": "响应成功",
    /*审核信息*/
    "1": "通过审核",
    "2": "驳回审核",
    /****************后台错误提示码****************/
    //活动和优惠券
    "-10701": "活动已启动不能修改",

    //公司管理
    "-10101": "公司查询失败",
    "-10102": "公司修改异常",
    "-10103": "公司新增异常",
    "-10104": "公司删除异常",
    "-10105": "公司禁用异常",
    "-10106": "公司启用异常",
    "-10107": "父公司为禁用状态，不能启用",

    //车辆
    "-10201": "车辆信息查询异常",
    "-10202": "车辆信息编辑异常",

    //司机审核
    "-10501": "司机审核列表查询失败",
    "-10502": "司机审核参数错误",
    "-10503": "司机审核失败",
    "-10504": "出租车司机证件审核失败",
    "-10505": "专车司机证件审核失败",

    //乘客
    "-10301": "乘客参数错误",
    "-10302": "乘客查询错误",
    "-10303": "乘客不存在",

    //城市服务
    "-10310": "此城市服务已被新增",


    "-10401": "司机查询列表失败",
    "-10402": "司机新增异常",
    "-10403": "司机删除异常",
    "-10404": "司机信息编辑异常",
    "-10405": "此司机的车辆已被注册",
    "-10406": "司机不存在",


    "-12101": "开始时间为空",
    "-12102": "截止时间为空",
    "-12103": "截止时间超过了当前日期",
    "-12104": "开始时间距当期日期超过一周，不能查询",

    "-12801": "车辆位置查询异常",
    "-12802": "车辆轨迹查询异常",
    "-12803": "服务类型查询异常",
    "-12804": "所选公司没有该服务",
    "-12805": "没有符合该条件的车辆",


    "-10601": "订单不存在",
    "-10602": "车辆位置异常",
    "-10603": "车辆品牌名已存在",
    "-10604": "角色名已存在",
    "-10605": "原密码不正确",
    "-10606": "此服务已存在",
    "-10607":"此车辆品牌的型号已存在",



    "-1": "系统错误",
    "-2": "通信错误,服务器无响应",
    "-3": "条件不满足，无结果",
    "-4": "参数错误",
    "-5": "系统关系型数据储错误",
    "-6": "系统mongodb数据储错误",
    "-7": "接入层服务器错误",
    "-8": "系统功能不支持",
    "-9": "未建立连接",
    "-10": "用户有正在进行中订单，不能重复下单",
    "-11": "订单不存在",
    "-12": "订单状态错误",
    "-14": "用户余额不足以支付",
    "-15": "支付类型不支持",
    "-16": "超出当日提现次数",
    "-17": "司机余额不足",
    "-18": "银行卡不存在",
    "-19": "该手机号司机已经存在",
    "-20": "车辆正处于维修状态",
    "-21": "司机已经存在",
    "-22": "系统功能不支持",
    "-23": "车牌号已存在",
    "-24": "身份证号已存在",
    "-43": "车辆不为启用状态，不予更换",
    "-26": "司机新增异常",
    "-25": "会话已过期",
    "-44": "OP错误",
    "-45": "公司ID为空",
    /*登录 错误码*/
    "-100": "用户未登录",
    "-101": "用户已登录",
    "-102": "用户不存在",
    "-103": "验证码为空",
    "-104": "认证失败",
    "-107": "用户名密码错误",
    "-106": "账户锁定",
    "-105": "密码不正确",
    "-108": "账户禁用",

    /*注册校验*/
    "-112": "登录名已存在",

    /*controller 错误码*/
    "-1001": "传参格式错误",
    "-1002": "参数为空",
    /****************资源管理****************/


    "-9999": "程序异常",

};